package com.dci.rest.model;

public class StyleBean {
	private String type;
	private String styleName;
	private String internalValue;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStyleName() {
		return styleName;
	}
	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}
	public String getInternalValue() {
		return internalValue;
	}
	public void setInternalValue(String internalValue) {
		this.internalValue = internalValue;
	}
}
