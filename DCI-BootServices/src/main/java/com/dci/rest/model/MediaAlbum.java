package com.dci.rest.model;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MediaAlbum<T> {
	private String id;
	private String name;
	private String description;
	private String type;
	private String size;
	private String createdBy;
	private String updatedBy;
	private String path;
	private String status;
	private String createTime;
	private String updateTime;
	private String comment;
	
	private String share;
		
	private String mediaToAlbum;
	private List<MediaBean> media;
	
	//DATABASE DATA FILTERING ATTRIBUTES
	
	private String childId;
	private String childName;
	
	private String parentId;
	private String level;
	private T albums;


	private String mediaId;
	private String mediaName;
	private String mediaStatus;
	private String mType;
	private String mPath;
	
	
	private String albumStr;

	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public List<MediaBean> getMedia() {
		return media;
	}
	public void setMedia(List<MediaBean> media) {
		this.media = media;
	}
	public String getMediaToAlbum() {
		return mediaToAlbum;
	}
	public void setMediaToAlbum(String mediaToAlbum) {
		this.mediaToAlbum = mediaToAlbum;
	}
	public String getChildId() {
		return childId;
	}
	public void setChildId(String childId) {
		this.childId = childId;
	}
	public String getChildName() {
		return childName;
	}
	public void setChildName(String childName) {
		this.childName = childName;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public T getAlbums() {
		return albums;
	}
	public void setAlbums(T albums) {
		this.albums = albums;
	}
	public String getMediaId() {
		return mediaId;
	}
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	public String getMediaName() {
		return mediaName;
	}
	public void setMediaName(String mediaName) {
		this.mediaName = mediaName;
	}
	public String getMediaStatus() {
		return mediaStatus;
	}
	public void setMediaStatus(String mediaStatus) {
		this.mediaStatus = mediaStatus;
	}
	public String getmType() {
		return mType;
	}
	public void setmType(String mType) {
		this.mType = mType;
	}
	public String getmPath() {
		return mPath;
	}
	public void setmPath(String mPath) {
		this.mPath = mPath;
	}
	public String getShare() {
		return share;
	}
	public void setShare(String share) {
		this.share = share;
	}
	public String getAlbumStr() {
		return albumStr;
	}
	public void setAlbumStr(String albumStr) {
		this.albumStr = albumStr;
	}


}
