package com.dci.rest.model;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@Component
public class Status {
	
	private int statusCode;
	private String status;
	private String message;
	private StringBuffer require;
	
	public final int getStatusCode() {
		return statusCode;
	}
	public final void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public final String getStatus() {
		return status;
	}
	public final void setStatus(String status) {
		this.status = status;
	}
	public final String getMessage() {
		return message;
	}
	public final void setMessage(String message) {
		this.message = message;
	}
	public final StringBuffer getRequire() {
		return require;
	}
	public final void setRequire(StringBuffer require) {
		this.require = require;
	}
	

}
