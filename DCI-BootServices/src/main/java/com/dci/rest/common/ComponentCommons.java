package com.dci.rest.common;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.Response;
import com.dci.rest.model.Status;
import com.dci.rest.utils.CheckString;
import com.dci.rest.utils.DBXmlManager;
import com.dci.rest.utils.StringUtility;

public class ComponentCommons {
	

	@Value("${VALID_NAME}" ) protected String validName;
	@Value("${INTERNAL_ERROR}" ) protected String internalServerError;
	@Value("${DUPLICATE_COMPONENT}" ) protected String duplicate;
	@Value("${NO_PREVILEGE}" ) protected String insuff_previlage;
	@Value("${DUPLICATE_FOOTNOTEID}" ) protected String duplicateFtId;
	@Value("${TEXT_TOO_LONG}" ) protected String textTooLong;
	@Value("${INVALID_COMPONENTID}" ) protected String invalidCompId;
	@Value("${OK}" ) protected String ok;
	@Value("${NOT_INDEPENDENT}" ) protected String notIndependent;
	@Value("${INVALID_PRIMARY_FLAG}" ) protected String invalidPrimaryFlg;
	@Value("${DUPLICATE_FILTER}" ) protected String duplicateFilter;

	
	
	
	
	private static String tags[] =   {"<footnoteref type=\"button\"","idref=\"","<input type=\"button\""};
	private static String tagsConversion[] =   {"<input type=\"image\"","src=\"htmlarea/images/footnote.gif\" title=\"","<input type=\"image\""};
	
	protected String processComponentBody(ComponentBean componentObj, Map<String, Object> otherParams , HttpServletRequest request,boolean editComponent){		
		//HttpServletRequest request = (HttpServletRequest) otherParams.get("request");
		String originalBody = componentObj.getBody();
		String componentType = componentObj.getType();
		boolean componentProcessingFlag = false;
		
		if(componentType.equalsIgnoreCase("para") || componentType.equalsIgnoreCase("bridgehead")){
			componentProcessingFlag = true;
		}
		if(componentProcessingFlag){
			originalBody = originalBody.replaceAll("\n", "\r\n").replaceAll("<p>", "<P> ").replaceAll("</p>", "</P>");
			originalBody = originalBody.replaceAll("\r\r", "\r");
		}
        String finalBody = originalBody;
        if(componentProcessingFlag){
        	finalBody = CheckString.replaceStringX(finalBody,request.getRequestURI(),"");
        } 
        finalBody = StringUtility.fixSpecialCharforWeb(finalBody);
        finalBody = CheckString.convertToXHTMLx(finalBody);
        finalBody = CheckString.convertStyleIndents(finalBody);
        
        if(componentProcessingFlag){
        	finalBody = finalBody.replaceAll("\r\n", "").trim();
        	finalBody = "<p>"+finalBody+"</p>";
        	finalBody = CheckString.removeCRLF(finalBody);
        }
        
        if (componentType.equals("footnote")) {
			if (finalBody.trim().indexOf("<p>") < 0){
				finalBody = "<footnote>" + finalBody + "</footnote>";
			}else if(!editComponent){
				finalBody = "<footnote>" + finalBody + "</footnote>";
			}
		} else if (componentType.equals("note")) {
			if (finalBody.trim().indexOf("<p>") < 0){
				finalBody = "<note>" + finalBody + "</note>";
			}else{
				finalBody = "<note>" + finalBody + "</note>";
			}
		} else if (componentType.equals("bridgehead")) {
			finalBody = "<bridgehead>" + finalBody + "</bridgehead>";
		}
        finalBody = CheckString.convertXHtmlToDocBook(finalBody);
        finalBody = CheckString.replaceStringX(finalBody, "<para/>", "");
		if (componentType.equals("para") || componentType.equals("note")){
			finalBody += "<para/>";
		}        
        return finalBody;
	}
	
	protected String getConvertedBody(String body,String type){
		if(CheckString.isValidString(body)){
			body = CheckString.xdocBookToHtml(body);
			if(CheckString.isValidString(type)) {
				if (body.indexOf("<bridgehead>") >-1){
					body = body.substring(body.indexOf("<bridgehead>") + "<bridgehead>".length(),body.indexOf("</bridgehead>"));
				}
				if(!type.equalsIgnoreCase("table")) {
					body = CheckString.stripOffhalfTag(body, "<para/>");
					body = CheckString.stripOffTag(body,"p");
					if("note".equals(type)){
						body = CheckString.stripOffTag(body, "note");
					}
					if("footnote".equals(type)){
						body = CheckString.stripOffTag(body, "footnote");
					}
					body = CheckString.removeCRLF(body);
					body = CheckString.converStyleIndentReverse(body);
				}				
			}
			
		}
		return body;
	}
	protected String getHiddenValues(String body, String entityType)throws Exception{
        String processedBody = CheckString.removeExtraSpaces(body);   
        String patternToCheck = "";     
        String patternToCheck_Second = "";      
        String tagName = "img";
        String attributeName = "id";
        
        if(entityType.equals("embedded")){
            patternToCheck = "<img id=\"[0-9]+\" alt=\"[^\"]+\" "+"src=\"htmlarea/images/embeddedobject.gif\" type=\"recursiveobj\" />";
        }else if(entityType.equals("footnote")){
            patternToCheck = "<footnoteref type=\"button\" alt=\"[0-9]+\" size=\"[0-9]+\" value=\"[0-9]+\" idref=\"[0-9]+\" order=\"[0-9]+\" />";
            patternToCheck_Second = "<footnoteref type=\"button\" alt=\"[0-9]+\" size=\"[0-9]+\" value=\"[0-9]+\" order=\"[0-9]+\" idref=\"[0-9]+\" />";
            tagName = "footnoteref";
            attributeName = "idref";
        }else if(entityType.equals("userVariable")){
            patternToCheck = "<img id=\"[0-9]+\" title=\"[^\"]+\" src=\"htmlarea/images/entity.gif\" type=\"entity\" "+"sequenceid=\"0\" condition=\"[a-z]+\" />";
        }else if(entityType.equals("systemVariable")){
            patternToCheck = "<img id=\"[0-9~-]+\" data-varscope=\"[^\"]+\"  title=\"[^\"]+\"  src=\"htmlarea/images/entity.gif\"  "+"type=\"sentity\" sequenceid=\"[0-9]+\" condition=\"[a-z]+\" />";
            //patternToCheck = "<img id=\"[0-9~-]+\" title=\"[^\"]+\" src=\"htmlarea/images/entity.gif\"  "+"type=\"sentity\" sequenceid=\"[0-9]+\" condition=\"[a-z]+\" />";
            patternToCheck_Second = "<img id=\"[0-9~-]+\" data-varscope=\"[^\"]+\" title=\"[^\"]+\" src=\"htmlarea/images/entity.gif\" "+"type=\"sentity\" sequenceid=\"[0-9]+\" condition=\"[a-z]+\" />";
        }
        
        String componentIds = "";
        if(entityType.equals("systemVariable")){
            int maxSeqId = 0;
             Pattern pattern = Pattern.compile(patternToCheck);
            Matcher matcher = pattern.matcher(processedBody);
            while(matcher.find()){
                Document documentObj;
                documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher.group()+"</root>");
                Element rootElement = documentObj.getDocumentElement();
                NodeList imageNode = rootElement.getElementsByTagName(tagName); 
                Element imageElement = (Element) imageNode.item(0);
                componentIds +="*"+imageElement.getAttribute(attributeName);
                int seqId = 0;
                if (imageElement.getAttribute("sequenceid") != null)
                    seqId = CheckString.getInt(imageElement.getAttribute("sequenceid"));
                if(maxSeqId < seqId)
                        maxSeqId = seqId;            
            }            
            Pattern pattern_second = Pattern.compile(patternToCheck_Second);
            Matcher matcher_second = pattern_second.matcher(processedBody);
            while(matcher_second.find()){
                Document documentObj;
                documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher_second.group()+"</root>");
                Element rootElement = documentObj.getDocumentElement();
                NodeList imageNode = rootElement.getElementsByTagName(tagName); 
                Element imageElement = (Element) imageNode.item(0);
                componentIds +="*"+imageElement.getAttribute(attributeName);
            
                int seqId = 0;
                if (imageElement.getAttribute("sequenceid") != null)
                    seqId = CheckString.getInt(imageElement.getAttribute("sequenceid"));
                if(maxSeqId < seqId)
                    maxSeqId = seqId;           
            }       
        }else if(entityType.equals("footnote")){
            Pattern pattern = Pattern.compile(patternToCheck);
            Matcher matcher = pattern.matcher(processedBody);
            while(matcher.find()){
                Document documentObj;
                documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher.group()+"</root>");
                Element rootElement = documentObj.getDocumentElement();
                NodeList imageNode = rootElement.getElementsByTagName(tagName); 
                Element imageElement = (Element) imageNode.item(0);
                componentIds +="*"+imageElement.getAttribute(attributeName);           
            }
            
            Pattern pattern_second = Pattern.compile(patternToCheck_Second);
            Matcher matcher_second = pattern_second.matcher(processedBody);
            while(matcher_second.find()){
                Document documentObj;
                documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher_second.group()+"</root>");
                Element rootElement = documentObj.getDocumentElement();
                NodeList imageNode = rootElement.getElementsByTagName(tagName); 
                Element imageElement = (Element) imageNode.item(0);
                componentIds +="*"+imageElement.getAttribute(attributeName);          
            }
        }else{
            Pattern pattern = Pattern.compile(patternToCheck);
            Matcher matcher = pattern.matcher(processedBody);
            while(matcher.find()){
                Document documentObj;
                documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher.group()+"</root>");
                Element rootElement = documentObj.getDocumentElement();
                NodeList imageNode = rootElement.getElementsByTagName(tagName); 
                //System.out.println(imageNode.getLength());
                Element imageElement = (Element) imageNode.item(0);
                componentIds +="*"+imageElement.getAttribute(attributeName);          
            }
            if(componentIds != null && componentIds.equals("")){
                String newPatternToCheck = "<img id=\"[0-9]+\"([\\s]?)alt=\"[^\"]+\"([\\s]?)src=\"htmlarea/images/embeddedobject.gif\"([\\s]?)type=\"recursiveobj\" />";
                Pattern newPattern = Pattern.compile(newPatternToCheck);
                Matcher newMatcher = newPattern.matcher(processedBody);
                while(newMatcher.find()){
                	String matchedString = "";
                    matchedString = newMatcher.group();
                    if(matchedString != null && !matchedString.equals("")){
                        String patternToCheckId = "id=\"[0-9]+\"([\\s]?)";
                        Pattern patternId = Pattern.compile(patternToCheckId);
                        Matcher matcherId = patternId.matcher(matchedString);
                        while(matcherId.find()){
                            String idStr = matcherId.group();
                            if(idStr != null && !idStr.equals("")){
                                idStr = idStr.replaceAll("id=", "");
                                idStr = idStr.replaceAll("\"", "");
                                idStr = idStr.replaceAll(" ", "");
                                componentIds +="*"+idStr;
                            }
                        }
                    }           
                }
            }
        }
        return componentIds==""?null:componentIds;
   }
	protected String changeFootnoteValues(String data){
	    
		data = CheckString.removeExtraSpaces(data);   
	    String patternToCheck = "";     
	    String tagName = "footnoteref";
	    patternToCheck = "<footnoteref type=\"button\" alt=\"[0-9]+\" size=\"[0-9]+\" value=\"[0-9]+\" order=\"[0-9]+\" />";
	    
	    Pattern pattern = Pattern.compile(patternToCheck);
	    Matcher matcher = pattern.matcher(data);
	    Document documentObj ;
	    while(matcher.find()){
	        try {
	            documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher.group()+"</root>");
	            Element rootElement = documentObj.getDocumentElement();
	            NodeList imageNode = rootElement.getElementsByTagName(tagName); 
	            
	            Element imageElement = (Element) imageNode.item(0);
	            String alt = imageElement.getAttribute("alt");
	            String order = imageElement.getAttribute("order");
	            String size = imageElement.getAttribute("size");
	            String value = imageElement.getAttribute("value");
	            String newString = "<footnoteref type=\"button\" alt=\""+alt+"\" size=\""+size+"\" value=\""+value+"\" idref=\""+value+"\" order=\""+order+"\" />";
	            data = data.replace(matcher.group(), newString);
	        } catch (Exception e) {
	            //new DCIException("com.dci.db4.dao.ComponentDAO || MethodName:changeFootnoteValues()",e);
	            e.printStackTrace();
	        }
	    }
	    
	    return data;
	}
	protected String convertTags(String source) {
		if (source == null || source.trim().length() ==0) return source;
		for(int i=0; i<tags.length;i++){
			source = CheckString.replaceStringX(source, tags[i], tagsConversion[i]);
		}
		return source;
	}
	

	
	protected String convertBlacklineContent(String result, String compType) {
		if (compType != null && compType.equals("Footnote")) {
			result = CheckString.stripOffTag(result, "footnote");
			result = CheckString.xdocBookToHtml(result);
		} else if (compType != null && compType.equals("Note")) {
			result = CheckString.stripOffTag(result, "note");
			result = CheckString.xdocBookToHtml(result);
		}
		result = CheckString.replaceStringX(result, "'", "&#39");
		result = CheckString.removeCRLF(result);
		result = CheckString.removeSectfromSearchResult(result);
		result = CheckString.xdocBookToHtml(result);
		result = CheckString.replaceStringX(result, "value=", " value=");
		result = CheckString.replaceStringX(result, "type=", " type=");
		result = CheckString.replaceStringX(result, "title=", " title=");
		result = CheckString.replaceStringX(result, "idref=", " idref=");
		result = CheckString.replaceStringX(result, "src=", " src=");
		result = CheckString.removeCRLF(result);	
		return result;
	}
	protected StringBuffer  getErrorAttributeList(BindingResult result){
		  StringBuffer require = new StringBuffer();
		int count=0;
		for (int i=0;i<result.getAllErrors().size();i++) {
			require.append(result.getFieldErrors().get(i).getField().toString());
			count++;
			if(count<result.getAllErrors().size())
			{
				require.append(","); 
			}
		}
		return require;
	}
	protected boolean validateClientDetails(HttpServletRequest request) {
		if(request.getParameter(("user"))!=null && request.getParameter("client")!=null){
			return true;
		}
		return false;
	}	
	// RESPONSE COMMON
	public Response<Object> validClientDetailsError()
	{
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		
		status.setStatusCode(401);
		status.setStatus("ERROR");
		status.setMessage("No User/ClientID was provided for basic authentication client ");
		status.setRequire(new StringBuffer("User Details"));
		responseVOTemp.setStatus(status);
		responseVOTemp.setResult("No User Data");	
		return responseVOTemp;
	}
	
	public Response<Object> validateRequiredField(BindingResult bindingResult,Object object){
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		status.setStatusCode(400);
		status.setStatus("ERROR");
		status.setMessage("Require Field");
		status.setRequire(getErrorAttributeList(bindingResult));
		responseVOTemp.setStatus(status);
		responseVOTemp.setResult(object);	
		return responseVOTemp;
	}
	public Response<Object> response(int statusCode,String statusStr ,String msg ,Object result){
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		status.setStatus(statusStr);
		status.setMessage(msg);
		status.setStatusCode(statusCode);
		responseVOTemp.setStatus(status);
		responseVOTemp.setResult(result);
		return responseVOTemp;
	}
	
	
}
