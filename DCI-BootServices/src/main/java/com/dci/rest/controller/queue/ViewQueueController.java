package com.dci.rest.controller.queue;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.model.Response;
import com.dci.rest.service.library.CommonService;

@RestController
@RequestMapping("${QUEUE}")
@CrossOrigin
public class ViewQueueController {
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	public Environment environment;

	private Logger developerLog = Logger.getLogger(ViewQueueController.class);
	
	@RequestMapping(value = "${QUEUE_PROOF}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getQueuedProofsList(HttpServletRequest request){
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = commonService.getQueuedProofsList(request);			
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.CurrentQueueController || Method Name : getQueuedProofsList() :"+e,e);
		}
		return responseVO;
		
	}
	@RequestMapping(value = "${QUEUE_CHARTS}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getQueuedChartList(HttpServletRequest request){
		Map<String,Object> reqParams = new HashMap<String,Object>();
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = commonService.getQueuedChartList(request);
			
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.CurrentQueueController || Method Name : getQueuedChartList() :"+e,e);
		}
		return responseVO;
		
	}
	
	@RequestMapping(value = "${QUEUE_CYCLE_DOC}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getCycleQueueDocuments(HttpServletRequest request){
		Map<String,Object> reqParams = new HashMap<String,Object>();
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = commonService.getCycleQueueDocuments(request);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.CurrentQueueController || Method Name : getCycleQueueDocuments() :"+e,e);
		}
		return responseVO;		
	}
	@RequestMapping(value = "${QUEUE_UPLOAD}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> getUploadQueue(HttpServletRequest request){
		Map<String,Object> reqParams = new HashMap<String,Object>();
		Response<Object> responseVO = new Response<Object>();
		try {			
			responseVO = commonService.getUploadQueue(request);
		} catch (Exception e) {
			developerLog.error("com.dci.rest.controller.library.CurrentQueueController || Method Name : getUploadQueue() :"+e,e);
		}
		return responseVO;		
	}
}
