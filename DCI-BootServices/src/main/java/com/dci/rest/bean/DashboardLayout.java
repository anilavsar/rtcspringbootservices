package com.dci.rest.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DashboardLayout {
	private String user;
	private String layoutId;
	private String chartOrder;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getLayoutId() {
		return layoutId;
	}
	public void setLayoutId(String layoutId) {
		this.layoutId = layoutId;
	}
	public String getChartOrder() {
		return chartOrder;
	}
	public void setChartOrder(String chartOrder) {
		this.chartOrder = chartOrder;
	}


}
