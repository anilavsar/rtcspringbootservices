package com.dci.rest.bean;
import java.util.*;

public class TableRow {
	ArrayList tablecell = null;
	ArrayList fieldattribute = null;
	private int cntCols;	

	public TableRow(){
		
	}
	
	public TableRow(ArrayList tablecell, ArrayList fieldattribute) {
		this.tablecell = tablecell;
		this.fieldattribute = fieldattribute;
	}

	/**
	 * @return
	 */
	public ArrayList getFieldattribute() {
		return fieldattribute;
	}

	/**
	 * @return
	 */
	public ArrayList getTablecell() {
		return tablecell;
	}

	/**
	 * @param ArrayList
	 */
	public void setFieldattribute(ArrayList collection) {
		fieldattribute = collection;
	}

	/**
	 * @param collection
	 */
	public void setTablecell(ArrayList collection) {
		tablecell = collection;
	}

	public String getAttributes() {
		StringBuffer sb = new StringBuffer(" ");
		if (fieldattribute == null) return "";
		for(int i=0;i<fieldattribute.size();i++) {
			FieldAttribute fa = (FieldAttribute)fieldattribute.get(i);
			if (fa != null) {
				sb.append(fa.getName());
				sb.append("=\"");
				sb.append(fa.getValue());
				sb.append("\" ");
			}
		} 
		return sb.toString();
	}
	
	/**
	 * @return
	 */
	public int getCntCols() {
		return cntCols;
	}

	/**
	 * @param i
	 */
	public void setCntCols(int i) {
		cntCols = i;
	}

}
