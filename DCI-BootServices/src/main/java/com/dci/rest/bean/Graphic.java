
package com.dci.rest.bean;
public class Graphic {
	private String fileref = null;
	private String width = null;
	private String height = null;
	private String charttype = null;

	/**
	 * @return
	 */
	public String getCharttype() {
		return charttype;
	}

	/**
	 * @return
	 */
	public String getFileref() {
		return fileref;
	}

	/**
	 * @return
	 */
	public String getHeight() {
		return height;
	}

	/**
	 * @return
	 */
	public String getWidth() {
		return width;
	}

	/**
	 * @param string
	 */
	public void setCharttype(String string) {
		charttype = string;
	}

	/**
	 * @param string
	 */
	public void setFileref(String string) {
		fileref = string;
	}

	/**
	 * @param string
	 */
	public void setHeight(String string) {
		height = string;
	}

	/**
	 * @param string
	 */
	public void setWidth(String string) {
		width = string;
	}

}
