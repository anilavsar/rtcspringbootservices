package com.dci.rest.bdo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dci.rest.common.ComponentCommons;
import com.dci.rest.dao.CommonDAO;
import com.dci.rest.dao.ComponentDAO;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.DepartmentBean;
import com.dci.rest.model.Response;

@Component
public class DepartmentBDO extends ComponentCommons {
	
	private Logger developerLog = Logger.getLogger(DepartmentBDO.class);
	@Autowired
	private CommonDAO commonDAO;
	
	public Response<Object> getDepartmentList(HttpServletRequest request){
		MDC.put("category","com.dci.rest.bdo.DepartmentBDO");
		developerLog.debug("Entering into com.dci.rest.bdo.DepartmentBDO || Method Name : getDepartmentList() ||");
		List<DepartmentBean> departmentList=null;		
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
		    departmentList=commonDAO.getDepartmentList();
		    return response(200,"SUCCESS","",departmentList);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.DepartmentBDO || getDepartmentList()",e);
			return response(500,"ERROR",internalServerError,null);
		}
	}
}
