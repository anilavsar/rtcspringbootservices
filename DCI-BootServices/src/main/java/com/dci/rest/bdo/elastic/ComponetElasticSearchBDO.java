package com.dci.rest.bdo.elastic;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import com.dci.rest.common.ComponentCommons;
import com.dci.rest.common.DciCommon;
import com.dci.rest.dao.ComponentDAO;
import com.dci.rest.dao.elastic.ComponentElasticDAO;
import com.dci.rest.dao.elastic.ComponentElasticDBDAO;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.SearchCriteria;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.Filter;
import com.dci.rest.model.Response;
import com.dci.rest.utils.CheckString;


@Component
public class ComponetElasticSearchBDO extends ComponentCommons{
	private Logger developerLog = Logger.getLogger(ComponetElasticSearchBDO.class);
	
	@Value("${ELASTIC_SEARCH_FLAG}" ) private String elasticServiceFlag;
	@Value("${RESULT_COUNT}" ) private String count;
	@Autowired ComponentElasticDAO searchDAO;
	@Autowired ComponentDAO compDAO;
	@Autowired ComponentElasticDBDAO compELDAO;

	public Response<Object> getSearchComponentList(SearchCriteria searchCriteria,HttpServletRequest request) {
			try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString().toLowerCase();
			String clientId = request.getParameter("client").toString();
			if (CheckString.isValidString(searchCriteria.getIsNew())) {
				if (searchCriteria.getIsNew().equals("true")
						&& CheckString.isValidString(searchCriteria.getElementInstanceIds())) {
					return compELDAO.getComponentESDATA(userName, clientId, searchCriteria.getElementInstanceIds());
				}
			}
			if (CheckString.isValidString(searchCriteria.getElementInstanceIds())) {
				String elementChildId[] = searchCriteria.getElementInstanceIds().split(",");
				if (elementChildId.length > 0) {
					String queryDsl = "{\"id\": \"getduplicatesearchtermtemplate\",\"params\": {\"id_list\":\"id_list\",\"id_list1\":\"id_list1\",\"value\":\""
							+ searchCriteria.getElementInstanceIds() + "\"}}";
					Map<String, String> result = searchDAO.getFilterContent(queryDsl,clientId);
					if (result.containsKey("success")) {
						Object obj = processESResultData(result);
						return response(200, "SUCCESS", "",obj);
					} else if (result.containsKey("error")) {
						return response(400, "ERROR", result.get("error"),"");
					}
				}
			}
			if(CheckString.isValidString(searchCriteria.getCreatedDate())) {
			    String queryDsl = "{\"id\": \"librarysearch\",\"params\": {\"created_on_param\":\"created_on_param\",\"datetime_created\":\"" + searchCriteria.getCreatedDate() + "\",\"size\":\"" + count + "\"}}";
			    Map<String, String> result= searchDAO.getFilterContent(queryDsl,clientId);
				if(result.containsKey("success")) {
					Object obj = processESResultData(result);
					return response(200, "SUCCESS", "",obj);
				}else if(result.containsKey("error")) {
					return response(400, "ERROR", result.get("error"),"");
				}
			}
			String searchParam = getFilterJSONFromReq(searchCriteria,count);	
			searchParam = searchParam.substring(1, searchParam.length()-1);
			searchParam = "{\"id\" : \"librarysearch\", "+searchParam+" }";
			Map<String, String> result= searchDAO.getComponentList(searchParam,clientId);
			if(result.containsKey("success")) {
			Object obj = processESResultData(result);
			return response(200, "SUCCESS", "",obj);
			}//else if(result.containsKey("error")) {
			return response(400, "ERROR", result.get("error"),"");
			//}			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.elastic.ComponetElasticSearchBDO || getSearchComponentList()", e);
			return response(500,"ERROR",internalServerError,searchCriteria);
		}
	}

	@SuppressWarnings("unchecked")
	private Object processESResultData(Map<String, String> result){
		Object obj = null;
		try {
			JSONParser parser = new JSONParser();
			obj = parser.parse(new StringReader(result.get("success")));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			org.json.simple.JSONArray resultObject = (org.json.simple.JSONArray) ((org.json.simple.JSONObject) jsonObject.get("hits")).get("hits");
			for (int idx = 0; idx < resultObject.size(); idx++) {
				org.json.simple.JSONObject componentObject = (org.json.simple.JSONObject) resultObject.get(idx);
				componentObject = (org.json.simple.JSONObject) componentObject.get("_source");
				String body = componentObject.get("element_detail").toString();
				String type = componentObject.get("component_type").toString();
				componentObject.put("oldFootnoteIds",getHiddenValues(body, "footnote"));
				componentObject.put("footnoteIds",getHiddenValues(body, "footnote"));

				componentObject.put("recursiveIds",getHiddenValues(body, "embedded"));
				componentObject.put("oldRecursiveIds",getHiddenValues(body, "embedded"));
				
				componentObject.put("entityIds",getHiddenValues(body, "userVariable"));
				componentObject.put("oldEntityIds",getHiddenValues(body, "userVariable"));
				
				componentObject.put("systemEntityIds",getHiddenValues(body, "systemVariable"));
				componentObject.put("oldSystemEntityIds",getHiddenValues(body, "systemVariable"));

				String finalbody = getConvertedBody(body, type);
				componentObject.put("element_detail", finalbody);
	      }
			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.elastic.ComponetElasticSearchBDO || processESResultData()", e);
		}
	  return obj;
	}
	private String getFilterJSONFromReq(SearchCriteria librarySearch,String count){		
	    JSONObject paramsParentJson = new JSONObject();
	    JSONObject paramsJson = new JSONObject();	    
	    try {	    	
    	    String component_type = CheckString.isValidString(librarySearch.getCompType())?librarySearch.getCompType():"";
		    String fcompstatus =  CheckString.isValidString(librarySearch.getStatus())?librarySearch.getStatus():"";
		    String assetclass_id =  CheckString.isValidString(librarySearch.getAssetClass())?librarySearch.getAssetClass():"";
		    String fund_association_id = CheckString.isValidString(librarySearch.getFunds())?librarySearch.getFunds():"";
		    String booktype_association_id =  CheckString.isValidString(librarySearch.getDocType())?librarySearch.getDocType():"";
		    String book_association_id =  CheckString.isValidString(librarySearch.getDocuments())?librarySearch.getDocuments():"";
		    String created_by =  CheckString.isValidString(librarySearch.getCreatedBy())?librarySearch.getCreatedBy().toLowerCase():"";
		    String last_changed_by =  CheckString.isValidString(librarySearch.getLastChangedBy())?librarySearch.getLastChangedBy().toLowerCase():"";
		    String fqualdata_context =  CheckString.isValidString(librarySearch.getContext())?librarySearch.getContext():"";
		    String flanguageid =  CheckString.isValidString(librarySearch.getLocale())?librarySearch.getLocale():"";
		    String searchData =  CheckString.isValidString(librarySearch.getSearchText())?librarySearch.getSearchText():"";
		    String noUsageSelection = CheckString.isValidString(librarySearch.getUsageFlag())?librarySearch.getUsageFlag():"";
		    String exactSearchSelection =  CheckString.isValidString(librarySearch.getExactSearchFlag())?librarySearch.getExactSearchFlag():"";
		    String effDate =  CheckString.isValidString(librarySearch.getEffDate())?librarySearch.getEffDate():"";
		    String expDate =  CheckString.isValidString(librarySearch.getExpDate())?librarySearch.getExpDate():"";		
		    String fcomp_assignee =  CheckString.isValidString(librarySearch.getFcomp_assignee())?librarySearch.getFcomp_assignee().toLowerCase():"";	
		    String ftagid =  CheckString.isValidString(librarySearch.getfTagId())?librarySearch.getfTagId():"";	
		    String fowner =  CheckString.isValidString(librarySearch.getFowner())?librarySearch.getFowner().toLowerCase():"";	

		    
		    getJSONArrayFromString(component_type,paramsJson,"component_type");
		   // getJSONArrayFromString(fcompstatus,paramsJson,"fcompstatus");
		    getJSONArrayFromString(fcompstatus,paramsJson,"fcompstatusid");
		    getJSONArrayFromString(assetclass_id,paramsJson,"assetclass_id");
		    getJSONArrayFromString(fund_association_id,paramsJson,"fund_association_id");
		    getJSONArrayFromString(booktype_association_id,paramsJson,"booktype_association_id");
		    getJSONArrayFromString(book_association_id,paramsJson,"book_association_id");
		    getJSONArrayFromString(created_by,paramsJson,"created_by");
		    getJSONArrayFromString(last_changed_by,paramsJson,"last_changed_by");
		    getJSONArrayFromString(fqualdata_context,paramsJson,"felementcontextid");
		    getJSONArrayFromString(flanguageid,paramsJson,"flanguageid");
		    getJSONArrayFromString(searchData,paramsJson,"searchData");
		    getJSONArrayFromString(effDate+"_"+expDate,paramsJson,"effDate_expDate");
		    getJSONArrayFromString(fcomp_assignee,paramsJson,"fcomp_assignee");
		    getJSONArrayFromString(ftagid,paramsJson,"felementtagids");
		    getJSONArrayFromString(fowner,paramsJson,"fowner");
		    if(CheckString.isValidString(exactSearchSelection) && exactSearchSelection.trim().equalsIgnoreCase("yes")){
		      getJSONArrayFromString(exactSearchSelection,paramsJson,"exact_search_param");
		    }
		    if(CheckString.isValidString(noUsageSelection) && noUsageSelection.trim().equalsIgnoreCase("yes")){
		      getJSONArrayFromString(noUsageSelection,paramsJson,"nousage_param");
		    }
		    
		    if(CheckString.isValidString(librarySearch.getCount())){
		        count = librarySearch.getCount();
		    }
		    paramsJson.put("size", count); 		    
		    paramsParentJson.put("params", paramsJson);
			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.elastic.ComponetElasticSearchBDO || getFilterJSONFromReq()", e);
		}
	    return paramsParentJson.toString();
	  }

	private void getJSONArrayFromString (String inpStr,JSONObject paramsJson, String keyS){	  
	  try {
			JSONArray compList = new JSONArray();
		    JSONObject innerJSON = new JSONObject();
		    if(keyS != null && keyS.equals("searchData") && inpStr != null && !inpStr.trim().equals("")){
		      innerJSON.put("search_term", inpStr);
		      paramsJson.put("search_term_param", innerJSON);
		    }else if(keyS != null && keyS.equals("exact_search_param")){
		      if(paramsJson.has("search_term_param")){
		        innerJSON = (JSONObject) paramsJson.get("search_term_param");
		        innerJSON.put("exact_search_param", "true");
		        paramsJson.put("search_term_param", innerJSON);
		      }
		    }else if(keyS != null && keyS.equals("nousage_param")){
		      paramsJson.put("nousage_param", "true");
		      if(paramsJson != null && paramsJson.length() > 0 && !paramsJson.has("criteria_param")){
		        paramsJson.put("criteria_param", "true");
		      }
		    }
		    else if(keyS != null && keyS.equals("effDate_expDate")){
			   	if(inpStr.length()>1){
				   	String effdate ="";
				   	String	expdate = "";
				   	String dateArr[] = inpStr.split("_");
				   	if(dateArr.length==1){
				   	effdate = dateArr[0];
				   	}else{
				   	effdate = dateArr[0];
				   	expdate = dateArr[1];
				   	}
				   	paramsJson.put("effectivexpired_param",true);
				   	
				   	if(effdate==null || effdate.equals("")){
				   	effdate = DciCommon.EFF_DATE;
				   	paramsJson.put("expiry_param", true);
				   	paramsJson.put("effectivexpired_param",false);
				   	}else if(expdate==null || expdate.equals("")){
				   	expdate = DciCommon.EXP_DATE;
				   	paramsJson.put("effective_param", true);
				   	paramsJson.put("effectivexpired_param",false);
				   	}
				   	paramsJson.put("effectivedate", effdate);
				   	paramsJson.put("expirydate", expdate);
				   	}
		    }else if(inpStr != null && !inpStr.trim().equals("")){
		      for(String listItems : Arrays.asList(inpStr.split(","))){
		        compList.put(listItems);
		      }
		      innerJSON.put(keyS, compList);
		      paramsJson.put(keyS+"_param", innerJSON);
		      if(paramsJson != null && paramsJson.length() >0 && !paramsJson.has("criteria_param")){
		        paramsJson.put("criteria_param", "true");
		      }
		  }		
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.elastic.ComponetElasticSearchBDO || getJSONArrayFromString()", e);
		}	   
	  }

	public Response<Object> getFilterContent(Map<String, String> reqParams,HttpServletRequest request) {
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String user = request.getParameter("user").toString().toLowerCase();
			String client = request.getParameter("client").toString();

			String filterName = reqParams.get("filterName");
			String filterSpecific = reqParams.get("filterSpecific");
			String contextId = reqParams.get("contextId");
			String contextIds = reqParams.get("contextIds");
			
			developerLog.debug("contextId:"+contextId);
			developerLog.debug("contextIds:"+contextIds);
			developerLog.debug("filterName:"+filterName);
			developerLog.debug("filterSpecific:"+filterSpecific);
			
			String queryDsl = "";
			
			
			if(CheckString.isValidString(filterName)) {
				if(filterName.equals("pendingwork")) {
				queryDsl = "{\"id\": \"dashboardtemplate\", \"params\": { \"size\": \"" + count+ "\",\"pendingwork\":\"pendingwork\",\"fund_association_id\":\"" + filterSpecific + "\",\"felementcontextid\":\""+ contextId + "\"}}";
				Map<String, String> result= searchDAO.getFilterContent(queryDsl,client);
				Object obj = processESResultData(result);
				return response(200, "SUCCESS", "",obj);
				}
			}
			ArrayList<Filter> filters = (ArrayList<Filter>) compDAO.getSystemFilterList(user, client);	
			System.out.println(filters);
			System.out.println("name"+filterName);
			filters.forEach(filter -> System.out.println(filter.getId()));
			//System.out.println("List:"+filters.forEach(item->System.out.println(item.)));
			if (CheckString.isValidString(filterName) && (filters.stream().anyMatch(filter -> filter.getId().trim().equalsIgnoreCase(filterName)))) {
				System.out.println("Inside If");
				if (filterName.equalsIgnoreCase("user")) {
					queryDsl = "{\"id\": \"dashboardtemplate\", \"params\": { \"userid\": \"" + user+ "\",\"userdashparam\":\"userdashparam\",\"size\":\"" + count + "\"}}";
				} else if (filterName.equalsIgnoreCase("recentUpdated")) {
					queryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"userid\": \"" + user
							+ "\",\"recentupdateddash\":\"recentupdateddash\",\"size\":\"" + count + "\"}}";
				} else if (filterName.equalsIgnoreCase("expired")) {
					queryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"expireddash\":\"expireddash\",\"size\":\""+ count + "\"}}";
				} else if (filterName.equalsIgnoreCase("expiresSoon")) {
					queryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"expiringsoondash\":\"expiringsoondash\",\"size\":\""+ count + "\"}}";
				} else if (filterName.equalsIgnoreCase("unused")) {
					queryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"unuseddash\":\"unuseddash\",\"size\":\""+ count + "\"}}";
				} else if (filterName.equalsIgnoreCase("fundspecific")) {
					String srchTerm = filterSpecific;
					queryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"fundassocdash\":\"fundassocdash\",\"fundid\":\""+ srchTerm + "\",\"size\":\"" + count + "\"}}";
				} else if (filterName.equalsIgnoreCase("elementcontext")&& CheckString.isValidString(contextIds)) {
					String srchTerm = filterSpecific;
					queryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"contextdash\":\"contextdash\",\"contextid\":"+ contextIds + ",\"size\":\"" + count + "\"}}";
				}
				else if (filterName.equalsIgnoreCase("elementcontext")) {
					String srchTerm = filterSpecific;
					queryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"contextdash\":\"contextdash\",\"contextid\":\""+ srchTerm + "\",\"size\":\"" + count + "\"}}";
				}
				else if (filterName.equalsIgnoreCase("bystatus")) {
					String srchTerm = filterSpecific;
					queryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"componentstatusid\":\"componentstatusid\",\"componentstatus\":\""+ srchTerm + "\",\"size\":\"" + count + "\"}}";
				}	
				else if (filterName.equalsIgnoreCase("assignedtome")) {
					String srchTerm = filterSpecific;
					queryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"fcomp_assignee\":\""+user+"\",\"assignedtome\":\"assignedtome\",\"size\":\"" + count + "\"}}";
				}
				else if (filterName.equalsIgnoreCase("quarterlyupdated")) {
					String srchTerm = filterSpecific;
					queryDsl = "{\"id\": \"dashboardtemplate\",\"params\": {\"userid\":\""+user+"\",\"quarterlyupdated\":\"quarterlyupdated\",\"size\":\"" + count + "\"}}";
				}
				Map<String, String> result= searchDAO.getFilterContent(queryDsl,client);
				if(result.containsKey("success")) {
					Object obj = processESResultData(result);
					return response(200, "SUCCESS", "",obj);
				}else if(result.containsKey("error")) {
					return response(400, "ERROR", result.get("error"),"");
				}
			} 
			return response(400, "ERROR", "invalid filter name",filterName);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.elastic.ComponetElasticSearchBDO || getFilterContent()", e);
			return response(500,"ERROR",internalServerError,reqParams);
		}
	}

	public Response<Object> getLibraryStatistics(Map<String, String> reqParams,HttpServletRequest request) {
		String queryDsl ="";
		String statType = reqParams.get("statType");
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString().toLowerCase();
			String clientId = request.getParameter("client").toString();

			if(statType == null) {
				queryDsl = "{\"size\": 0,\"aggregations\": {\"filter_deactivated_components\" : {\"filter\":{\"bool\":{\"must_not\":{\"term\":{\"fcompstatus\":4}}}},\"aggregations\": {\"docused\": {\"terms\": {\"field\": \"document_used\"}},\"comp_type\": {\"terms\": {\"field\": \"component_type.raw\"}},\"monthly_content\" : {\"date_histogram\" : {\"field\" : \"datetime_created\",\"interval\" : \"day\",\"format\" : \"yyyy-MM-dd\"}},\"element_context\": {\"terms\": {\"field\": \"felementcontext_desc.raw\"}},\"created_by\": {\"terms\": {\"field\": \"created_by\"}},\"expired\" : {\"filters\" : {\"filters\" : {\"expired_components\" :   {\"range\":{\"expiration_date\":{\"lt\":\"now\"}}}}}},\"updated_last_yr\" : {\"filters\" : {\"filters\" : {\"least_updated\" :   {\"range\":{\"datetime_last_changed\":{\"lt\":\"now-1y\"}}}}}},\"one_versus_many\": {\"terms\": {\"field\": \"document_used_count\"}}}},\"status\": {\"terms\": {\"field\": \"fcompstatus\"}}}}";
			}else if(statType.equalsIgnoreCase("status")) {
				queryDsl = "{\"size\": 0,\"aggregations\": {\"status\": {\"terms\": {\"field\": \"fcompstatus\"}}}}";
			}else if(statType.equalsIgnoreCase("comp_type")) {
				queryDsl = "{\"size\": 0,\"aggregations\": {\"filter_deactivated_components\" :{\"filter\":{\"bool\":{\"must_not\":{\"term\":{\"fcompstatus\":4}}}},\"aggregations\": {\"comp_type\": {\"terms\": {\"field\": \"component_type.raw\"}}}}}}";
			}else if(statType.equalsIgnoreCase("monthly_content")) {
				queryDsl = "{\"size\": 0,\"aggregations\": {\"filter_deactivated_components\" :{\"filter\":{\"bool\":{\"must_not\":{\"term\":{\"fcompstatus\":4}}}},\"aggregations\": {\"monthly_content\" : {\"date_histogram\" : {\"field\" : \"datetime_created\",\"interval\" : \"day\",\"format\" : \"yyyy-MM-dd\"}}}}}}";
			}else if(statType.equalsIgnoreCase("element_context")) {
				queryDsl = "{\"size\": 0,\"aggregations\": {\"filter_deactivated_components\" :{\"filter\":{\"bool\":{\"must_not\":{\"term\":{\"fcompstatus\":4}}}},\"aggregations\": {\"element_context\": {\"terms\": {\"field\": \"felementcontext_desc.raw\"}}}}}}";
			}else if(statType.equalsIgnoreCase("created_by")) {
				queryDsl = "{\"size\": 0,\"aggregations\": {\"filter_deactivated_components\" :{\"filter\":{\"bool\":{\"must_not\":{\"term\":{\"fcompstatus\":4}}}},\"aggregations\": {\"created_by\": {\"terms\": {\"field\": \"created_by\"}}}}}}";
			}else if(statType.equalsIgnoreCase("expired")) {
				queryDsl = "{\"size\": 0,\"aggregations\": {\"filter_deactivated_components\" :{\"filter\":{\"bool\":{\"must_not\":{\"term\":{\"fcompstatus\":4}}}},\"aggregations\": {\"expired\" : {\"filters\" : {\"filters\" : {\"expired_components\" :   {\"range\":{\"expiration_date\":{\"lt\":\"now\"}}}}}}}}}}";
			}else if(statType.equalsIgnoreCase("updated_last_yr")) {
				queryDsl = "{\"size\": 0,\"aggregations\": {\"filter_deactivated_components\" :{\"filter\":{\"bool\":{\"must_not\":{\"term\":{\"fcompstatus\":4}}}},\"aggregations\": {\"updated_last_yr\" : {\"filters\" : {\"filters\" : {\"least_updated\" :   {\"range\":{\"datetime_last_changed\":{\"lt\":\"now-1y\"}}}}}}}}}}";
			}else if(statType.equalsIgnoreCase("docused")) {
				queryDsl = "{\"size\": 0,\"aggregations\": {\"filter_deactivated_components\" :{\"filter\":{\"bool\":{\"must_not\":{\"term\":{\"fcompstatus\":4}}}},\"aggregations\": {\"docused\": {\"terms\": {\"field\": \"document_used\"}}}}}}";
			}
			Map<String, String> result = searchDAO.getLibraryStatistics(queryDsl,clientId);
			if(result.containsKey("success")) {				
				JSONParser parser = new JSONParser();
				return response(200, "SUCCESS", "",(org.json.simple.JSONObject) parser.parse(result.get("success")));
			}//else if(result.containsKey("error")) {
				return response(400, "ERROR", result.get("error"),"");
		//}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.elastic.ComponetElasticSearchBDO || getLibraryStatistics()", e);
			return response(500,"ERROR",internalServerError,reqParams);
		}
	}
	public Response<Object> getLinkageComponent(Map<String,String> reqParams,HttpServletRequest request) throws Exception{
	    developerLog.debug("Entering into com.dci.rest.bdo.ComponetElasticSearchBDO || Method Name : getLinkageComponent() ||");                
		String sourceCompId = null,queryDsl=null;
		Map<String, String>  result =null;
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
		//	String userName = request.getParameter("user").toString();
			String clientId = request.getParameter("client").toString();
			sourceCompId = reqParams.get("sourceCompId");
		  if (!CheckString.isNumericValue(sourceCompId)) {
			  return response(400, "ERROR", "invalid sourceCompId path param", sourceCompId);
			}
			ComponentBean componentBean =  compDAO.getComponentData(sourceCompId);
			if (componentBean.getType().equals("footnote") || componentBean.getType().equals("graphic")|| componentBean.getType().equals("bridgehead") || componentBean.getType().equals("table") || componentBean.getType().equals("para")) {
				
				queryDsl = "{\"id\": \"dashboardtemplate\", \"params\": {\"component_type\": \""+ componentBean.getType() + "\", \"felementinstanceid\": \""+ sourceCompId + "\",\"linkagecomponents\":\"linkagecomponents\",\"size\":\"" + count + "\"}}";
				
				result = searchDAO.getFilterContent(queryDsl,clientId);
				
				if (result.containsKey("success")) {
					JSONParser parser = new JSONParser();
					return response(200, "SUCCESS", "",(org.json.simple.JSONObject) parser.parse(result.get("success")));
				} else if (result.containsKey("error")) {
					return response(400, "ERROR", result.get("error"),"");
				}
			} 
				return response(500, "SUCCESS", "Component Type does not match", "success");
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.elastic.ComponetElasticSearchBDO || getLinkageComponent()", e);
		 	return response(500,"ERROR",internalServerError,reqParams);
		} 
	}
	public Response<Object> searchLinkageComponent(SearchCriteria searchCriteria, BindingResult bindingResult,Map<String,String> reqParams,HttpServletRequest request) throws Exception{
	    developerLog.debug("Entering into com.dci.rest.bdo.ComponetElasticSearchBDO || Method Name : searchLinkageComponent() ||");                
	    String sourceCompId = null;
	    List<ComponentBean> searchResultlist;
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			if (bindingResult.hasErrors()) {
				return validateRequiredField(bindingResult,searchCriteria);
			}

			String userName = request.getParameter("user").toString().toLowerCase();
			String clientId = request.getParameter("client").toString();
			sourceCompId = reqParams.get("sourceCompId");
			if (!CheckString.isNumericValue(sourceCompId)) {
				return response(400, "ERROR", "invalid sourceCompId path param",searchCriteria);
			}
			boolean isPrimary = Boolean.parseBoolean(searchCriteria.getIsFromPrimary());
			if (isPrimary) {
				searchCriteria.setIsFromPrimary("1");
			} else {
				searchCriteria.setIsFromPrimary("0");
			}
			searchCriteria.setComponentId(sourceCompId);
			searchResultlist = new ArrayList<ComponentBean>();
			searchResultlist = compDAO.searchLinkageComponent(userName, clientId, searchCriteria);
			return response(200, "SUCCESS", "",searchResultlist);

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.elastic.ComponetElasticSearchBDO || searchLinkageComponent()", e);
			return response(500,"ERROR",internalServerError,searchCriteria);
		} 
	}
	public Response<Object> searchduplicateComponent(ComponentBean component,BindingResult bindingresult,HttpServletRequest request) throws Exception{
	    developerLog.debug("Entering into com.dci.rest.bdo.ComponetElasticSearchBDO || Method Name : searchduplicateComponent() ||");                
		String compId = "";
		JSONParser parser = new JSONParser();
		try {
		    if(!validateClientDetails(request)) {
		    	return validClientDetailsError();
		    }
			String userName = request.getParameter("user").toString().toLowerCase();
			String clientId = request.getParameter("client").toString();
			String operation = "";
			if (component.getOperation() != null) {
				if (component.getOperation().equals("suggestions")) {
					operation = component.getOperation();
				}
			}
			if(component.getElementInstanceId()!=null){
				compId = component.getElementInstanceId();
			}
			Map<String, String> result = searchDAO.httpGetDuplicateSearch(operation,component.getBody(),component.isNoDiff(),compId,component.getConfidenceScore(),clientId);
			 developerLog.debug("EXIT searchduplicateComponent()  ComponetElasticSearchBDO ");
			if(result.containsKey("success")) {
				return response(200, "SUCCESS", "",(org.json.simple.JSONArray) parser.parse(result.get("success")));
			}//else if(result.containsKey("error")) {
				return response(400, "ERROR", result.get("error"),component);
			//}			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.elastic.ComponetElasticSearchBDO || searchduplicateComponent()", e);
			return response(500,"ERROR",internalServerError,component);
		}
	}

	public Response<Object> searchDuplicateComponentByKey(String key, ComponentBean component,BindingResult bindingresult, HttpServletRequest request) throws Exception {
		developerLog.debug(
				"Entering into com.dci.rest.bdo.ComponetElasticSearchBDO || Method Name : searchduplicateComponent() ||");
		Map<String, String> result = null;
		int score = 0;
		String compId = "";
		try {
			if (!validateClientDetails(request)) {
				return validClientDetailsError();
			}
			String userName = request.getParameter("user").toString().toLowerCase();
			String clientId = request.getParameter("client").toString();
			if (key != null) {
				compId = key;
			}
			if (component.getConfidenceScore() != null) {
				score = Integer.parseInt(component.getConfidenceScore());
			}

			if (score > 0) {
				result = searchDAO.httpGetDuplicateByKey(compId, score,clientId);
			}
			if (result.containsKey("success")) {
				JSONParser parser = new JSONParser();
				return response(200, "SUCCESS", "", (org.json.simple.JSONArray) parser.parse(result.get("success")));
			} //else if (result.containsKey("error")) {
				return response(400, "ERROR", "", result);
			//}

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.elastic.ComponetElasticSearchBDO || searchDuplicateComponentByKey()",e);
			return response(500, "ERROR", internalServerError, component);
		}
	}

}
