package com.dci.rest.dao.elastic;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.dci.rest.dao.DataAccessObject;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.Response;
import com.dci.rest.model.Status;
import com.dci.rest.utils.CheckString;
import com.dci.rest.utils.DBXmlManager;
import com.dci.rest.utils.DateFormat;
@Repository
@Component
public class ComponentElasticDBDAO extends DataAccessObject {
	
	private Logger developerLog = Logger.getLogger(ComponentElasticDBDAO.class);

	public JSONObject getComponent(String user, String client, String componentId) {
		MDC.put("category", "com.dci.rest.dao.ComponentElasticDBDAO");
		developerLog.debug("Entering into com.dci.rest.dao.ComponentElasticDBDAO || Method Name : getComponent() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		JSONObject paramsParentJson = new JSONObject();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}				
			String elementChildId[] = componentId.split(",");
			if (elementChildId.length > 0) {
				for (int i = 0; i < elementChildId.length; i++) {
					
					developerLog.debug("call SP8_RTC_SEARCHCOMPONENTSBYIDS('" + user + "'," + client + "," + componentId + ")");
					cs = con.prepareCall("{call SP8_RTC_SEARCHCOMPONENTSBYIDS(?,?,?)}");
					cs.setString(1, user);
					cs.setBigDecimal(2, CheckString.getBigDecimal(client));
					cs.setString(3, componentId);
					rs = cs.executeQuery();
					while (rs.next()) {
						JSONObject resultJson = new JSONObject();
						resultJson.put("fcomp_status",rs.getString("FSTATUS_ID"));
						resultJson.put("fcompstatusid", rs.getString("WRK_FLOWSTATUSID"));
						resultJson.put("type", "component");
						resultJson.put("expiration_date", DateFormat.expandedDate(rs.getDate("fexpirationdate")));
						if(rs.getDate("fexpirationdate")==null) {
							resultJson.put("expiration_date", "");
						}
						resultJson.put("effective_date", DateFormat.expandedDate(rs.getDate("feffectivedate")));
						if(rs.getDate("feffectivedate")==null) {
							resultJson.put("feffectivedate", "");
						}
						resultJson.put("qual_data_desc", rs.getString("fqualdata_desc"));
						resultJson.put("felementcontextid", rs.getString("felementcontextid"));
						if(rs.getString("felementcontextid")==null) {
							resultJson.put("felementcontextid", "-1");
						}
						resultJson.put("felementcontext_desc", rs.getString("FELEMENTCONTEXT_DESC"));
						resultJson.put("component_type", rs.getString("felement_id"));
						resultJson.put("felementinstanceid", rs.getString("felementinstanceid"));
						paramsParentJson.put("_source", resultJson);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentElasticDBDAO || getComponent()", e);
		} 
		return paramsParentJson;

	}
	
	public Response<Object> getComponentESDATA(String userName,String client,String componentId) {
		MDC.put("category", "com.dci.rest.bdo.ComponentElasticDBDAO");
		Response<Object> responseVOTemp = new Response<Object>();
		Status status = new Status();
		JSONObject componentList;
		JSONParser parser = new JSONParser();
		try {
			componentList = getComponent(userName, client, componentId);
			status.setStatusCode(200);
			status.setStatus("SUCESS");
			responseVOTemp.setStatus(status);
			responseVOTemp.setResult((org.json.simple.JSONObject) parser.parse(crtESResponse(componentList).toString()));
			return responseVOTemp;

		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentElasticDBDAO || getComponent()", e);
			return responseVOTemp;
		}
	}

	protected JSONObject crtESResponse(JSONObject componentList) {
		JSONObject paramsParentJsonOuter = new JSONObject();
		JSONObject paramsParentJson = new JSONObject();
		JSONObject paramsParentJsonInner = new JSONObject();
		JSONObject resultJson = new JSONObject();
		try {
			resultJson.put("_index", "evg_dev");
			resultJson.put("_type", "component");
			resultJson.put("_source",componentList.get("_source"));
			paramsParentJsonInner.put("hits", resultJson);
			paramsParentJson.put("hits", paramsParentJsonInner);
			paramsParentJsonOuter.put("result",paramsParentJson);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.bdo.ComponentElasticDBDAO || crtESResponse()", e);
		}
		return paramsParentJson;

	}
protected String getHiddenValues(String body, String entityType)throws Exception{
	        String processedBody = CheckString.removeExtraSpaces(body);   
	        String patternToCheck = "";     
	        String patternToCheck_Second = "";      
	        String tagName = "img";
	        String attributeName = "id";
	        
	        if(entityType.equals("embedded")){
	            patternToCheck = "<img id=\"[0-9]+\" alt=\"[^\"]+\" "+"src=\"htmlarea/images/embeddedobject.gif\" type=\"recursiveobj\" />";
	        }else if(entityType.equals("footnote")){
	            patternToCheck = "<footnoteref type=\"button\" alt=\"[0-9]+\" size=\"[0-9]+\" value=\"[0-9]+\" idref=\"[0-9]+\" order=\"[0-9]+\" />";
	            patternToCheck_Second = "<footnoteref type=\"button\" alt=\"[0-9]+\" size=\"[0-9]+\" value=\"[0-9]+\" order=\"[0-9]+\" idref=\"[0-9]+\" />";
	            tagName = "footnoteref";
	            attributeName = "idref";
	        }else if(entityType.equals("userVariable")){
	            patternToCheck = "<img id=\"[0-9]+\" title=\"[^\"]+\" src=\"htmlarea/images/entity.gif\" type=\"entity\" "+"sequenceid=\"0\" condition=\"[a-z]+\" />";
	        }else if(entityType.equals("systemVariable")){
	            patternToCheck = "<img id=\"[0-9~-]+\" data-varscope=\"[^\"]+\"  title=\"[^\"]+\"  src=\"htmlarea/images/entity.gif\"  "+"type=\"sentity\" sequenceid=\"[0-9]+\" condition=\"[a-z]+\" />";
	            //patternToCheck = "<img id=\"[0-9~-]+\" title=\"[^\"]+\" src=\"htmlarea/images/entity.gif\"  "+"type=\"sentity\" sequenceid=\"[0-9]+\" condition=\"[a-z]+\" />";
	            patternToCheck_Second = "<img id=\"[0-9~-]+\" data-varscope=\"[^\"]+\" title=\"[^\"]+\" src=\"htmlarea/images/entity.gif\" "+"type=\"sentity\" sequenceid=\"[0-9]+\" condition=\"[a-z]+\" />";
	        }
	      
	        String componentIds = "";
	        if(entityType.equals("systemVariable")){
	            int maxSeqId = 0;
	             Pattern pattern = Pattern.compile(patternToCheck);
	            Matcher matcher = pattern.matcher(processedBody);
	            while(matcher.find()){
	                Document documentObj;
	                documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher.group()+"</root>");
	                Element rootElement = documentObj.getDocumentElement();
	                NodeList imageNode = rootElement.getElementsByTagName(tagName); 
	                Element imageElement = (Element) imageNode.item(0);
	                componentIds +="*"+imageElement.getAttribute(attributeName);
	                int seqId = 0;
	                if (imageElement.getAttribute("sequenceid") != null)
	                    seqId = CheckString.getInt(imageElement.getAttribute("sequenceid"));
	                if(maxSeqId < seqId)
	                        maxSeqId = seqId;            
	            }            
	            Pattern pattern_second = Pattern.compile(patternToCheck_Second);
	            Matcher matcher_second = pattern_second.matcher(processedBody);
	            while(matcher_second.find()){
	                Document documentObj;
	                documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher_second.group()+"</root>");
	                Element rootElement = documentObj.getDocumentElement();
	                NodeList imageNode = rootElement.getElementsByTagName(tagName); 
	                Element imageElement = (Element) imageNode.item(0);
	                componentIds +="*"+imageElement.getAttribute(attributeName);
	            
	                int seqId = 0;
	                if (imageElement.getAttribute("sequenceid") != null)
	                    seqId = CheckString.getInt(imageElement.getAttribute("sequenceid"));
	                if(maxSeqId < seqId)
	                    maxSeqId = seqId;           
	            }       
	        }else if(entityType.equals("footnote")){
	            Pattern pattern = Pattern.compile(patternToCheck);
	            Matcher matcher = pattern.matcher(processedBody);
	            while(matcher.find()){
	                Document documentObj;
	                documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher.group()+"</root>");
	                Element rootElement = documentObj.getDocumentElement();
	                NodeList imageNode = rootElement.getElementsByTagName(tagName); 
	                Element imageElement = (Element) imageNode.item(0);
	                componentIds +="*"+imageElement.getAttribute(attributeName);           
	            }
	            
	            Pattern pattern_second = Pattern.compile(patternToCheck_Second);
	            Matcher matcher_second = pattern_second.matcher(processedBody);
	            while(matcher_second.find()){
	                Document documentObj;
	                documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher_second.group()+"</root>");
	                Element rootElement = documentObj.getDocumentElement();
	                NodeList imageNode = rootElement.getElementsByTagName(tagName); 
	                Element imageElement = (Element) imageNode.item(0);
	                componentIds +="*"+imageElement.getAttribute(attributeName);          
	            }
	        }else{
	            Pattern pattern = Pattern.compile(patternToCheck);
	            Matcher matcher = pattern.matcher(processedBody);
	            while(matcher.find()){
	                Document documentObj;
	                documentObj = DBXmlManager.getDOMFromXMLString("<root>"+matcher.group()+"</root>");
	                Element rootElement = documentObj.getDocumentElement();
	                NodeList imageNode = rootElement.getElementsByTagName(tagName); 
	                //System.out.println(imageNode.getLength());
	                Element imageElement = (Element) imageNode.item(0);
	                componentIds +="*"+imageElement.getAttribute(attributeName);          
	            }
	            if(componentIds != null && componentIds.equals("")){
	                String newPatternToCheck = "<img id=\"[0-9]+\"([\\s]?)alt=\"[^\"]+\"([\\s]?)src=\"htmlarea/images/embeddedobject.gif\"([\\s]?)type=\"recursiveobj\" />";
	                Pattern newPattern = Pattern.compile(newPatternToCheck);
	                Matcher newMatcher = newPattern.matcher(processedBody);
	                while(newMatcher.find()){
	                	String matchedString = "";
	                    matchedString = newMatcher.group();
	                    if(matchedString != null && !matchedString.equals("")){
	                        String patternToCheckId = "id=\"[0-9]+\"([\\s]?)";
	                        Pattern patternId = Pattern.compile(patternToCheckId);
	                        Matcher matcherId = patternId.matcher(matchedString);
	                        while(matcherId.find()){
	                            String idStr = matcherId.group();
	                            if(idStr != null && !idStr.equals("")){
	                                idStr = idStr.replaceAll("id=", "");
	                                idStr = idStr.replaceAll("\"", "");
	                                idStr = idStr.replaceAll(" ", "");
	                                componentIds +="*"+idStr;
	                            }
	                        }
	                    }           
	                }
	            }
	        }
	        return componentIds==""?null:componentIds;
	   }
		protected String getConvertedBody(String body,String type){
			if(CheckString.isValidString(body)){
				body = CheckString.xdocBookToHtml(body);
				if(CheckString.isValidString(type)) {
					if (body.indexOf("<bridgehead>") >-1){
						body = body.substring(body.indexOf("<bridgehead>") + "<bridgehead>".length(),body.indexOf("</bridgehead>"));
					}
					if(!type.equalsIgnoreCase("table")) {
						body = CheckString.stripOffhalfTag(body, "<para/>");
						body = CheckString.stripOffTag(body,"p");
						if("note".equals(type)){
							body = CheckString.stripOffTag(body, "note");
						}
						if("footnote".equals(type)){
							body = CheckString.stripOffTag(body, "footnote");
						}
						body = CheckString.removeCRLF(body);
						body = CheckString.converStyleIndentReverse(body);
					}				
				}
				
			}
			return body;
		}
}
