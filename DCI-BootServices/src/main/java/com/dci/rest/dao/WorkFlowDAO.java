package com.dci.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.ComponentStatus;
import com.dci.rest.model.Schema;
import com.dci.rest.model.Transition;
import com.dci.rest.model.WorkFLStatus;
import com.dci.rest.utils.CheckString;

@Component
@Repository
public class WorkFlowDAO extends DataAccessObject {
	
	private Logger developerLog = Logger.getLogger(WorkFlowDAO.class);

	
	public List<Schema> getAllSchemaList(String userName,String schemaId) {
		MDC.put("category","com.dci.rest.dao.WorkFlowDAO");
		developerLog.debug("Entering into com.dci.db4.dao.WorkFlowDAO || Method Name : getAllSchemaList() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ComponentStatus compStatus;
		Schema schema;
		List<Schema> schemaList = new ArrayList<Schema>();
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{call SP_RTC_GETSCHEMASTATUS ("+userName+","+schemaId+")}");
			cs = con.prepareCall("{call SP_RTC_GETSCHEMASTATUS (?,?)}");	
			cs.setString(1, userName);
			cs.setString(2, schemaId);
			rs = cs.executeQuery();

			while (rs != null && rs.next()) {
				schema = new Schema();
				schema.setId(CheckString.getInt(rs.getString("FSCHEMAID")));
				schema.setName(rs.getString("FSCHEMA_NAME"));
				schema.setDescription(rs.getString("FSCHEMA_DESC"));
				schema.setType(rs.getString("FSCHEMA_TYPE"));
				schema.setfStatus(rs.getString("FCOMPSTATUS"));
				//schema.setsId(CheckString.getInt(rs.getString("FSTATUSID")));
				//schema.setfStatusDesc(rs.getString("FSTATUS_DESC"));
				schema.setfStatusDesc(rs.getString("FCOMPSTATUS_DESC"));
				//schema.setfStatusName(rs.getString("FSTATUS_ID"));//
				schema.setfStatusName(rs.getString("FCOMP_STATUS"));//
				schema.setfOrder(rs.getString("FORDER"));
				schema.setStatusType(rs.getString("FSTATUS_TYPE"));
				schema.setCreator(rs.getString("FCREATEDBY"));
				schema.setfStatusAssigee(rs.getString("FASSIGNEE"));//
				schemaList.add(schema);
			}
		} catch (Exception e) {
			e.printStackTrace();
			new DocubuilderException("Exception in com.dci.rest.dao.WorkFlowDAO || getAllSchemaList()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.WorkFlowDAO || Method Name : getAllSchemaList() ||");
		}

		return schemaList;
	}

	
	public List<Transition> getStatusTransiton(String userName,int statusId,int order) {
		MDC.put("category","com.dci.rest.dao.WorkFlowDAO");
		developerLog.debug("Entering into com.dci.db4.dao.WorkFlowDAO || Method Name : getStatusTransiton() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ComponentStatus compStatus;
		Transition transition;
		List<Transition> transitionList = new ArrayList<Transition>();
		Map<String, String> assigneeMap = new HashMap<String, String>();
		
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{call SP8_RTC_GETSCHEMASTATUSTRANS ("+userName+","+statusId+","+order+")}");
			cs = con.prepareCall("{call SP8_RTC_GETSCHEMASTATUSTRANS (?,?,?)}");	
			cs.setString(1, userName);
			cs.setInt(2, statusId);
			cs.setInt(3, order);
			rs = cs.executeQuery();

			while (rs.next()) {
				transition = new Transition();
				transition.setId(CheckString.getInt(rs.getString("FSTATUSTOID")));
				transition.setName(rs.getString("TRANS_DESC"));
				//transition.setsId(CheckString.getInt(rs.getString("FSTATUSID")));
				//transition.setsId(CheckString.getInt(rs.getString("FCOMPSTATUS")));

				transitionList.add(transition);
			}
		} catch (Exception e) {
			e.printStackTrace();
			new DocubuilderException("Exception in com.dci.rest.dao.WorkFlowDAO || getStatusTransiton()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.WorkFlowDAO || Method Name : getStatusTransiton() ||");
		}

		return transitionList;
	}
	public String createEditSchema(String userName,String clientId,String schemaId,Schema schema) {
		MDC.put("category","com.dci.rest.dao.WorkFlowDAO");
		developerLog.debug("Entering into com.dci.db4.dao.WorkFlowDAO || Method Name : createEditSchema() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		String updateStatus = null;
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{call SP8_RTC_SETSCHEMA("+userName+","+clientId+","+schemaId+","+schema.getName()+","+schema.getDescription()+","+schema.getType()+",?)}");
			cs = con.prepareCall("{call SP8_RTC_SETSCHEMA(?,?,?,?,?,?,?)}");	
			cs.setString(1, userName);
			cs.setInt(2, CheckString.getInt(clientId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(schemaId));
			cs.setString(4, schema.getName());
			cs.setString(5, schema.getDescription());
			cs.setInt(6, CheckString.getInt(schema.getType()));
			cs.registerOutParameter(7, Types.VARCHAR);
			cs.executeUpdate();
			updateStatus = cs.getString(7);
		} catch (Exception e) {
			e.printStackTrace();
			new DocubuilderException("Exception in com.dci.rest.dao.WorkFlowDAO || createSchema()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.WorkFlowDAO || Method Name : createSchema() ||");
		}
		return updateStatus;
	}
	public String editSchemaTransition(String userName,String clientId,String schemaId,int id,String transitionIds) {
		MDC.put("category","com.dci.rest.dao.WorkFlowDAO");
		developerLog.debug("Entering into com.dci.db4.dao.WorkFlowDAO || Method Name : editSchemaTransition() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		String updateStatus = null;
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{call SP8_RTC_UPDATESTATUSTRANSITION("+userName+","+clientId+","+schemaId+","+id+","+transitionIds+")}");
			cs = con.prepareCall("{call SP8_RTC_UPDATESTATUSTRANSITION(?,?,?,?,?)}");	
			cs.setString(1, userName);
			cs.setInt(2, CheckString.getInt(clientId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(schemaId));
			cs.setBigDecimal(4, CheckString.getBigDecimal(id));
			cs.setString(5, transitionIds);
			cs.executeUpdate();
			updateStatus="success";
		} catch (Exception e) {
			e.printStackTrace();
			new DocubuilderException("Exception in com.dci.rest.dao.WorkFlowDAO || editSchemaTransition()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.WorkFlowDAO || Method Name : editSchemaTransition() ||");
		}
		return updateStatus;
	}
	public String editSchemaStatus(String userName,String clientId,int bigDecimal,String name,String desciption,String assignee,int order,String schemaId,WorkFLStatus workFLStatus) {
		MDC.put("category","com.dci.rest.dao.WorkFlowDAO");
		developerLog.debug("Entering into com.dci.db4.dao.WorkFlowDAO || Method Name : editSchemaStatus() ||");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		String updateStatus = null;
		try {
			if (con == null || con.isClosed()) {
				con = getConnection();
			}
			developerLog.debug("{call SP8_RTC_UPDATESTATUS("+userName+","+clientId+","+workFLStatus.getId()+","+name+","+desciption+","+assignee+","+order+","+schemaId+")}");
			cs = con.prepareCall("{call SP8_RTC_UPDATESTATUS(?,?,?,?,?,?,?,?,?)}");	
			cs.setString(1, userName);
			cs.setInt(2, CheckString.getInt(clientId));
			cs.setBigDecimal(3,  CheckString.getBigDecimal(workFLStatus.getId()));
			cs.setString(4, name);
			cs.setString(5, desciption);
			cs.setString(6,  assignee);
			cs.setBigDecimal(7, CheckString.getBigDecimal(order));
			cs.setBigDecimal(8, CheckString.getBigDecimal(schemaId));
			cs.registerOutParameter(9, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			updateStatus = cs.getString(9);
		} catch (Exception e) {
			e.printStackTrace();
			new DocubuilderException("Exception in com.dci.rest.dao.WorkFlowDAO || editSchemaStatus()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.db4.dao.WorkFlowDAO || Method Name : editSchemaStatus() ||");
		}
		return updateStatus;
	}
	 
}

