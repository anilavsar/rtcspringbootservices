package com.dci.rest.dao.auth;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.dci.rest.dao.DataAccessObject;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.ComponentPermissionBean;
import com.dci.rest.utils.CheckString;
@Component
@Repository
public class UserPrivilegeDAO extends DataAccessObject{
	private Logger developerLog = Logger.getLogger(UserPrivilegeDAO.class);
	@Value("${APP_ID}" ) private String appId;
	
	
	public Map<String, ComponentPermissionBean> getComponentPrivilege(String user, String client) {
		developerLog.warn("Entering into com.dci.rest.dao.auth.UserPrivilegeDAO || Method Name : getComponentPrivilege() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con= null;
		Map<String, ComponentPermissionBean> compPermissions = new HashMap<String, ComponentPermissionBean>();
		try {
			if (con == null || con.isClosed()){
				con = getAuthConnection();
			}
			developerLog.debug("call SP8_DSGETUSERPERMISION('"+ user+"','"+client+"','"+appId+"')");
			cs = con.prepareCall("{call SP8_DSGETUSERPERMISION(?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, CheckString.getInt(client));
			cs.setInt(3, CheckString.getInt(appId));
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				if(CheckString.isValidString(rs.getString("FOBJSCHEMA_DESC")) && rs.getString("FOBJSCHEMA_DESC").equalsIgnoreCase("Component Permissions")) {
					if(compPermissions.containsKey(rs.getString("FDCIOBJECT_DESC").toLowerCase())) {
						ComponentPermissionBean userPrivilegeBean = compPermissions.get(rs.getString("FDCIOBJECT_DESC").toLowerCase());
						userPrivilegeBean.setPermissionLevel(userPrivilegeBean.getPermissionLevel()+1);
						userPrivilegeBean.getPermission().add(rs.getString("FPRIV_OPTION_DESC"));
						compPermissions.replace(rs.getString("FDCIOBJECT_DESC").toLowerCase(), userPrivilegeBean);
					}else {
						ComponentPermissionBean userPrivilegeBean = new ComponentPermissionBean();
						userPrivilegeBean.setType(rs.getString("FDCIOBJECT_DESC"));
						userPrivilegeBean.getPermission().add(rs.getString("FPRIV_OPTION_DESC"));
						userPrivilegeBean.setPermissionLevel(0);
						compPermissions.put(rs.getString("FDCIOBJECT_DESC").toLowerCase(), userPrivilegeBean);
					}
				}
			}
			
		}catch(Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.authO || getComponentPrivilege()",e);

		}finally {
			releaseConStmts(rs, cs, con, "com.dci.rest.dao.auth.UserPrivilegeDAO || Method Name : getComponentPrivilege()");
		}
		return compPermissions;
	}

}
