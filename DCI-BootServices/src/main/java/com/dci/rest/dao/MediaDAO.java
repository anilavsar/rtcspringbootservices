package com.dci.rest.dao;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.dci.rest.bdo.MediaBDO;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.APIModel;
import com.dci.rest.model.AssetClass;
import com.dci.rest.model.DocumentBean;
import com.dci.rest.model.DocumentType;
import com.dci.rest.model.Fund;
import com.dci.rest.model.MediaBean;
import com.dci.rest.model.Tag;
import com.dci.rest.model.WorkFLStatus;
import com.dci.rest.utils.CheckString;
import com.dci.rest.utils.DateFormat;

@Component
@Repository
public class MediaDAO extends DataAccessObject{
	

	private Logger developerLog = Logger.getLogger(MediaBDO.class);
	
	public Map<String, String> createMedia(String user,String client,MediaBean media) {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Map<String,String> result = new HashMap<String,String>();
		String createStatus = null;String id = null;
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : createMedia() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP9_RTC_CREATEMEDIA ("+user+","+client+","+media.getDescription()+","+media.getType()+","+media.getPath()+","+media.getStatus()+","+media.getName()+","+media.getAlt()+","+media.getSize()+"?,?)}");
			cs = con.prepareCall("{call SP9_RTC_CREATEMEDIA  (?,?,?,?,?,?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3, media.getDescription());
			cs.setBigDecimal(4, CheckString.getBigDecimal(media.getType()));
			cs.setString(5, media.getPath());
			cs.setBigDecimal(6, CheckString.getBigDecimal(media.getStatus()));
			cs.setString(7, media.getName());
			cs.setString(8, media.getAlt());
			cs.setBigDecimal(9, CheckString.getBigDecimal(media.getSize()));
			cs.registerOutParameter(10, Types.VARCHAR);
			cs.registerOutParameter(11, java.sql.Types.BIGINT);
			cs.executeUpdate();
			createStatus = cs.getString(10);
			id = cs.getString(11);
		    result.put("createStatus", createStatus);
		    result.put("id", id);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || createMedia()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : createMedia() ||");
		}
		return result;	
		
	}
	public String createMediaTag(String user,String client,String mediaId,String name) {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;  
		Map<String,String> result = new HashMap<String,String>();
		String createStatus = null;String id = null;
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : createMediaTag() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP9_RTC_CREATEMETATAG ("+user+","+client+","+name+",?)}");
			cs = con.prepareCall("{call SP9_RTC_CREATEMETATAG  (?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3, name);
			cs.registerOutParameter(4, java.sql.Types.BIGINT);
			cs.executeUpdate();
			createStatus = cs.getString(4);
			id = cs.getString(4);
		    //result.put("id", id);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || createMediaTag()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : createMediaTag() ||");
		}
		return id;	
		
	}
	public String associateMediaTag(String user,String client,String mediaId,int tagIds) {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;  
		Map<String,String> result = new HashMap<String,String>();
		String tagAssociation = null;String id = null;
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : associateMediaTag() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP9_RTC_ASSOCIATE_MEDIA2TAG ("+user+","+client+","+mediaId+","+tagIds+",?)}");
			cs = con.prepareCall("{call SP9_RTC_ASSOCIATE_MEDIA2TAG  (?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3, mediaId);
			cs.setString(4, Integer.toString(tagIds));
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.executeUpdate();
			tagAssociation = cs.getString(5);
			//id = cs.getString(5);
		    //result.put("id", id);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || associateMediaTag()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : associateMediaTag() ||");
		}
		return tagAssociation;	
		
	}
	public String disassociateMediaTag(String user,String client,String mediaId,String tagIds) {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;  
		Map<String,String> result = new HashMap<String,String>();
		String tagDisassociation = null;String id = null;
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : disassociateMediaTag() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP9_RTC_DISASSOCIATE_MEDIA2TAG  ("+user+","+client+","+mediaId+","+tagIds+",?)}");
			cs = con.prepareCall("{call SP9_RTC_DISASSOCIATE_MEDIA2TAG   (?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3, mediaId);
			cs.setBigDecimal(4, CheckString.getBigDecimal(tagIds));
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.executeUpdate();
			tagDisassociation = cs.getString(5);
			//id = cs.getString(5);
		    //result.put("id", id);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || disassociateMediaTag()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : disassociateMediaTag() ||");
		}
		return tagDisassociation;	
		
	}
	public Map<String, String> editMedia(String user,String client,String mediaId,MediaBean media) {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		Map<String,String> result = new HashMap<String,String>();
		String createStatus = null;String id = null;
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : editMedia() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP9_RTC_UPDATEMEDIA ("+user+","+client+","+media.getId()+","+media.getName()+","+media.getStatus()+","+media.getType()+","+media.getMessage()+","+media.getPath()+"?)}");
			cs = con.prepareCall("{call SP9_RTC_UPDATEMEDIA  (?,?,?,?,?,?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setBigDecimal(3, CheckString.getBigDecimal(mediaId));
			cs.setString(4, media.getName());
			cs.setString(5, media.getDescription());
			cs.setString(6, media.getPath());
			cs.setBigDecimal(7, CheckString.getBigDecimal(media.getStatus()));
			cs.setString(8 ,media.getAlt());
			cs.setBigDecimal(9, CheckString.getBigDecimal(media.getSize()));
			cs.registerOutParameter(10, Types.VARCHAR);
			cs.executeUpdate();
			createStatus = cs.getString(10);
		    result.put("updateStatus", createStatus);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || editMedia()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : editMedia() ||");
		}
		return result;	
		
	}	
	
	
	
	public List<MediaBean> getAllMedia(String user, String client, String mediaId) {
		MDC.put("category"," com.dci.rest.dao.MediaDAO");
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : getAllMedia() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		MediaBean mediaBean = null;
		Tag tagBean = null;
		List<MediaBean> mediaBeanList = new ArrayList<MediaBean>();
		List<Tag> taglist = null;
		 try {
			con = getConnection();
			developerLog.debug("{CALL SP9_RTC_GETMEDIA ("+user+","+ client+","+ mediaId+")}");
			cs = con.prepareCall("{CALL SP9_RTC_GETMEDIA (?,?,?)}");
			cs.setBigDecimal(1, CheckString.getBigDecimal(client));
			cs.setString(2, user);
			cs.setBigDecimal(3, CheckString.getBigDecimal(mediaId));
			rs = cs.executeQuery();
			while(rs.next()) {
				mediaBean = new MediaBean();
				mediaBean.setName(rs.getString("FMEDIANAME"));
				mediaBean.setDescription(rs.getString("FMEDIADESC"));
				mediaBean.setStatus(rs.getString("FMEDIASTATUS"));
				mediaBean.setType(rs.getString("FMEDIATYPEID"));
				mediaBean.setMessage(rs.getString("FERRMSG"));
				mediaBean.setPath(rs.getString("FFILELOCATION"));
				mediaBean.setAlt(rs.getString("FALT"));
				mediaBean.setSize(rs.getInt("FSIZE")+"");
				mediaBean.setId(rs.getString("FMEDIAID"));
				
				mediaBean.setCreatedBy(rs.getString("FCREATEDBY"));
				mediaBean.setCreateTime(rs.getString("FTIMECREATED"));
				mediaBean.setUpdatedBy(rs.getString("FLASTCHANGEDBY"));
				mediaBean.setUpdateTime(rs.getString("FTIMELASTCHANGED"));
				
				String tagId = rs.getString("FMETATAGID");
				if(CheckString.isValidString(rs.getString("FMETATAGID"))){
					mediaBean.setTagId(tagId);
					mediaBean.setTagName(rs.getString("FMETATAGDESC"));
				}				

				/*
				if(CheckString.isValidString(mediaId))*/
				//defaultAssingee.setAssignee(rs.getString("FASSIGNEE"));
				mediaBeanList.add(mediaBean);
			}
		 }catch (Exception e){
				new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || getAllMedia()",e);
			} finally {
				releaseConStmts(null, cs, con, null);
				developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : getAllMedia()");
			}	
		 return mediaBeanList;
	}
	
	public String removeMedia(String user,String client,String mediaId) {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;  
		Map<String,String> result = new HashMap<String,String>();
		String createStatus = null;String id = null;
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : removeMedia() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP9_RTC_REMOVEMEDIA  ("+user+","+client+","+mediaId+",?)}");
			cs = con.prepareCall("{call SP9_RTC_REMOVEMEDIA   (?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(client));
			cs.setString(3,mediaId);
			cs.registerOutParameter(4, java.sql.Types.BIGINT);
			cs.executeUpdate();
			createStatus = cs.getString(4);
			id = cs.getString(4);
		    //result.put("id", id);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || removeMedia()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : removeMedia() ||");
		}
		return id;	
		
	}
	
	public ArrayList<DocumentBean> getMediaDocumentAssociation(String user, String clientId, String componentId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ArrayList<DocumentBean> DocList = new ArrayList<DocumentBean>();
		try {
			con = getConnection();
			developerLog.debug("{call sp_RTC_utGetBookInstanceList(" + user + ","+ clientId + ","+906507+")}");
			cs = con.prepareCall("{call sp_RTC_utGetBookInstanceList(?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, new Integer(clientId).intValue());
			if(CheckString.isValidString(componentId)) {
				cs.setBigDecimal(3, CheckString.getBigDecimal(906507));
			}else {
				cs.setNull(3, Types.NULL);
			}			
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				DocumentBean document = new DocumentBean();
				document.setId(rs.getString("fbookinstanceid"));
				document.setName(rs.getString("fbookinstance_description"));
				document.setEffDate(DateFormat.expandedDate(rs.getDate("feffectivedate")));
				document.setExpDate(DateFormat.expandedDate(rs.getDate("fexpirationdate")));
				document.setStatusId(rs.getString("fbookinstance_status"));
				document.setStatusDesc(rs.getString("fstatus_description"));				
				document.setDetails((document.getName() + " ("+ document.getStatusDesc() + ")"));				
				DocList.add(document);
			}
		} catch (Exception e) {			
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || getMediaDocumentAssociation()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : getMediaDocumentAssociation() ||");
		}
		return DocList;
	}
	public List<DocumentType> getMediaDocTypeAssociation(String user, String clientId,String elementInstanceId) throws Exception {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO|| Method Name : getMediaDocTypeAssociation() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		List<DocumentType> docTypeList = new ArrayList<DocumentType> ();
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			
			developerLog.debug("{call SP_RTC_UTGETELEMENTBKTYPELIST('" + user+ "'," + clientId + "," + 906507 + ")}");
			cs = con.prepareCall("{call SP_RTC_UTGETELEMENTBKTYPELIST(?,?,?)}");
			cs.setString(1, user);
			cs.setInt(2, CheckString.getInt(clientId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(906507));
			rs = cs.executeQuery();
			while (rs.next()) {
				DocumentType docType = new DocumentType(rs.getString("fbooktypeId"),null, rs.getString("fbook_type"));
				docTypeList.add(docType);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || getMediaDocTypeAssociation()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO|| Method Name : getMediaDocTypeAssociation() ||");
		}
		return docTypeList;
	}
	public List<AssetClass> getMediaAssetClassAssociation(String user, String clientId, String componentId) {
		MDC.put("category","com.dci.rest.dao.ComponentDAO");
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO|| Method Name : getMediaAssetClassAssociation() ||");
		List<AssetClass> assetClassList = new ArrayList<AssetClass>();
		CallableStatement cs = null;		  
		ResultSet rs = null;
		Connection con = null;
		try {
			if(con == null || con.isClosed()) {
				con = getConnection();
			}
		  	developerLog.debug("SP8_RTC_GETCOMPONENTASSETCLASS('"+user+"',"+clientId+","+ 906507 +")");
			cs = con.prepareCall("{call SP8_RTC_GETCOMPONENTASSETCLASS(?,?,?)}");
			cs.setString(1, user);	
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setBigDecimal(3,CheckString.getBigDecimal(906507));
			rs = cs.executeQuery();
			while(rs != null && rs.next()) {
				AssetClass assetClass = new AssetClass(rs.getString("fassetclassid"), rs.getString("fassetclass"));
				assetClassList.add(assetClass);
			}
		  } catch (Exception e) {
			  new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || getMediaAssetClassAssociation()",e);
		  } finally {
			  releaseConStmts(rs, cs, con,null); 
			  developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : getMediaAssetClassAssociation() ||");
		  }	
		return  assetClassList;		
	}
	public ArrayList<Fund> getMediaContentList(String user, String clientId, String componentId) {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		ArrayList<Fund> FundsArray = new ArrayList<Fund>();
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			
			developerLog.debug("{call SP8_RTC_UTGETCONTENTLIST('" + user + "',"+ clientId + ","+906507+")}");
			cs = con.prepareCall("{call SP8_RTC_UTGETCONTENTLIST('" + user + "',"+ clientId + ","+906507+")}");
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				Fund fund = new Fund();
				fund.setId(rs.getInt("fcontentid"));
				fund.setName(rs.getString("fcontent_id"));
				//fund.setAssetClassId(rs.getString("FCONTENTCATEGORYID"));
				fund.setAssetClassName(rs.getString("FCONTENTCATEGORY_DESCRIPTION"));
				FundsArray.add(fund);
			}
		} catch (Exception e) {			
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || getMediaContentList()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : getMediaContentList() ||");
		}
		return FundsArray;
	}
	public List<Fund> getFundListOnAssetClass(String user, String clientId, String assetClassIds){
		List<Fund> fundList = new ArrayList<Fund>();
		boolean local = true;
		Connection con = null;
		CallableStatement cs = null;		  
		ResultSet rs = null;
		 try {
			BigDecimal bd = null;
			if (con == null || con.isClosed())
				con = getConnection();
			else
				local = false;
			developerLog.debug("{CALL SP4_RTC_GETFUNDSBYASSETCLASS('"+user+"',"+clientId+",'"+assetClassIds+"')}");
			cs = con.prepareCall("{CALL SP4_RTC_GETFUNDSBYASSETCLASS(?,?,?)}");
			cs.setString(1, user);	
			cs.setInt(2, new Integer(clientId).intValue());
			cs.setString(3,assetClassIds);
			rs = cs.executeQuery();
			while(rs.next()) {
				Fund mb = new Fund();
				mb.setId(rs.getInt(1));
				mb.setsId(rs.getInt(1)+"*"+rs.getString(2));
				mb.setDescription(rs.getString(3));
				mb.setValue(mb.getId()+"");
				mb.setLabel(mb.getDescription());	
				mb.setName(rs.getString(3));
				mb.setAssetClassId(rs.getInt(4)+"");
				mb.setAssetClassName(rs.getString("FCONTENTCATEGORY_DESCRIPTION"));		
				fundList.add(mb);
			}			
		 } catch (SQLException e) {
				new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || getFundListOnAssetClass()",e);
			} finally {
				releaseConStmts(null, cs, con,null);
			}

	   return fundList;		
	}
	public String dissociateMediaComponent(String user, String clientId, String elementInsId)throws Exception {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : dissociateMediaComponent() ||");
		CallableStatement cs = null;
		Connection con = null;
		String status="";
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("call SP8_RTC_ITDISSOCIATE_ELEMENT('" + user+ "'," + clientId + "," + elementInsId + ")");
			cs = con.prepareCall("{call SP8_RTC_ITDISSOCIATE_ELEMENT(?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(clientId));
			cs.setBigDecimal(3, CheckString.getBigDecimal(elementInsId));
			cs.executeUpdate(); 
			status="success";
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || dissociateMediaComponent()",e);
			return "error";
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : dissociateMediaComponent() ||");
		}
		return status;
	}
	public String setMediaDocumentAssociation(String user,String elementInstanceId, String bookInstanceId) throws Exception {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : setMediaDocumentAssociation() ||");
		CallableStatement cs = null;
		Connection con = null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			
			System.out.println("call SP8_RTC_itAssociate_Element2Book("+ user + "," + elementInstanceId + ",'"+ bookInstanceId+ "')");
			cs = con.prepareCall("{call sp8_RTC_itAssociate_Element2Book(?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementInstanceId));
			cs.setString(3,bookInstanceId);
			cs.executeUpdate();
		} catch (Exception e) {
			new DocubuilderException("Exception in ccom.dci.rest.dao.MediaDAO || setMediaDocumentAssociation()",e);
		} finally {
			releaseConStmts(null, cs, con,null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO ||MethodName : setMediaDocumentAssociation ||");
		}
		return "success";
	}
	public String setMediaDocTypeAssociation(String user, String elementId, String docTypeIds)throws Exception {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : setMediaDocTypeAssociation() ||");
		CallableStatement cs = null;
		Connection con = null;
		String status="";
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			System.out.println("call sp_RTC_itAssociate_Element2BKType('" + user+ "'," + elementId + ",'" + docTypeIds + "',?)");
			cs = con.prepareCall("{call sp_RTC_itAssociate_Element2BKType(?,?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementId));
			cs.setString(3, docTypeIds);
			cs.registerOutParameter(4, Types.VARCHAR);
			cs.executeUpdate();
			status = (cs.getString(4).equals("0")) ? "success":"error";			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || setMediaDocTypeAssociation()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : setMediaDocTypeAssociation() ||");
		}
		return status;
	}
	public String setMediaAssetClassAssociation(String user,String elementInstanceId, String contentCategoryId,String bodytype)throws Exception {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : setMediaAssetClassAssociation() ||");
		CallableStatement cs = null;
		Connection con = null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			
			System.out.println("call SP_RTC_ITMAPCOMP2ASSETCLASS("+ user+ ","+ elementInstanceId+ ","+ contentCategoryId+ "," + bodytype +  ")");
			cs = con.prepareCall("{call SP_RTC_ITMAPCOMP2ASSETCLASS(?,?,?,?)}");
			cs.setString(1, user);
			cs.setString(2, elementInstanceId);
			cs.setString(3, contentCategoryId);
			cs.setString(4,bodytype);
			cs.executeUpdate();
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || setMediaAssetClassAssociation()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO ||setMediaAssetClassAssociation ||");
		}
		return "success";
	}
	public String setMediaFundAssociation(String user,String elementInstanceId, String contentId)throws Exception {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : setMediaFundAssociation() ||");
		CallableStatement cs = null;
		Connection con =null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			System.out.println("call sp8_RTC_itAssociate_Element2Content("+ user + "," + elementInstanceId + ",'"+ contentId + "')");
			cs = con.prepareCall("{call sp8_RTC_itAssociate_Element2Content(?,?,?)}");
			cs.setString(1, user);
			cs.setBigDecimal(2, CheckString.getBigDecimal(elementInstanceId));
			cs.setString(3, contentId);
			cs.executeUpdate();
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || setMediaFundAssociation()",e);
		} finally {
			releaseConStmts(null, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : setMediaFundAssociation() ||");
		}
		return "success";
	}
	
	public Map<String, String> createMediaAPI(String user,String client,APIModel apiModel) {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;  
		Map<String,String> result = new HashMap<String,String>();
		String createStatus = null;String id = null;
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : createMediaAPI() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call sp8_RTC_PUTMEDIAAPI("+user+","+","+apiModel.getName()+","+apiModel.getKeyId()+","+apiModel.getKeySecret()+"?)}");
			cs = con.prepareCall("{call sp8_RTC_PUTMEDIAAPI(?,?,?,?,?)}");
			cs.setString(1, user);
			cs.setString(2, apiModel.getName());
			cs.setString(3, apiModel.getKeyId());
			cs.setString(4, apiModel.getKeySecret());
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.executeUpdate();
			createStatus = cs.getString(5);
			//id = cs.getString(4);
		    //result.put("id", id);
			result.put("createStatus", createStatus);
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || createMediaAPI()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : createMediaAPI() ||");
		}
		return result;	
	}
	

	public List<APIModel> getMediaAPI(String user,String client) {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;  
		APIModel apiModel;
		List<APIModel> mediaApiList = new ArrayList<APIModel>();
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : getMediaAPI() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_GETMEDIAAPI  ("+user+",?)}");
			cs = con.prepareCall("{call SP8_RTC_GETMEDIAAPI   (?,?)}");
			cs.setString(1, user);	
			cs.setString(2, null);
			rs = cs.executeQuery();
			while(rs.next()) {
				apiModel = new APIModel();
				apiModel.setName(rs.getString("FAPINAME"));
				//apiModel.setKeyId(rs.getString("FKEY_ID"));
				//apiModel.setKeySecret(rs.getString("FKEY_SECRET"));
				mediaApiList.add(apiModel);
			}			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || getMediaAPI()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : getMediaAPI() ||");
		}
		return mediaApiList;	
		
	}
	public APIModel getPostMediaAPI(String user,String client,String apiName) {
		MDC.put("category","com.dci.rest.dao.MediaDAO");
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;  
		APIModel apiModel = null;
		List<APIModel> mediaApiList = new ArrayList<APIModel>();
		developerLog.debug("Entering into com.dci.rest.dao.MediaDAO || Method Name : getPostMediaAPI() ||");
		try {
			if(con == null || con.isClosed())
				con = getConnection();
			developerLog.debug("{call SP8_RTC_GETMEDIAAPI  ("+user+","+apiName+")}");
			cs = con.prepareCall("{call SP8_RTC_GETMEDIAAPI   (?,?)}");
			cs.setString(1, user);	
			cs.setString(2, apiName);	
			rs = cs.executeQuery();
			while(rs.next()) {
				apiModel = new APIModel();
				apiModel.setName(rs.getString("FAPINAME"));
				apiModel.setKeyId(rs.getString("FKEY_ID"));
				apiModel.setKeySecret(rs.getString("FKEY_SECRET"));
			}			
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.MediaDAO || getPostMediaAPI()",e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.MediaDAO || Method Name : getPostMediaAPI() ||");
		}
		return apiModel;	
		
	}
}
