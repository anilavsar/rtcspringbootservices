package com.dci.rest.service.library;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.dci.rest.bdo.elastic.ComponetElasticSearchBDO;
import com.dci.rest.model.SearchCriteria;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.Response;
import com.dci.rest.utils.CheckString;

@Service
public class ComponentSearchService {
	@Value("${ELASTIC_SEARCH_FLAG}" ) private String elasticServiceFlag;
	@Autowired
	private ComponetElasticSearchBDO searchBDO;
	
	public Response<Object> searchComponents(SearchCriteria searchCriteria, HttpServletRequest request) {
		Response<Object> response=new Response<Object>();
		if(CheckString.isValidString(elasticServiceFlag)&& Boolean.valueOf(elasticServiceFlag)) {
			response = searchBDO.getSearchComponentList(searchCriteria, request) ;
		}else {
			
		}
		return response;
	}
	
	public Response<Object> getFilterContent(Map<String, String> reqParams,HttpServletRequest request) {
		Response<Object> response=new Response<Object>();
		if(CheckString.isValidString(elasticServiceFlag)&& Boolean.valueOf(elasticServiceFlag)) {
			response = searchBDO.getFilterContent(reqParams,request) ;
		}else {
			
		}
		return response;
	}

	public Response<Object> getLibraryStatistics(Map<String, String> reqParams,HttpServletRequest request) {
		Response<Object> response=new Response<Object>();
		if(CheckString.isValidString(elasticServiceFlag)&& Boolean.valueOf(elasticServiceFlag)) {
			response = searchBDO.getLibraryStatistics(reqParams,request) ;
		}else {
			
		}
		return response;
	}
	public Response<Object> searchLinkageComponent(SearchCriteria searchCriteria, BindingResult bindingresult,Map<String,String> reqParams,HttpServletRequest request) throws Exception {
		Response<Object> response=new Response<Object>();
		if(false /*CheckString.isValidString(elasticServiceFlag)&& Boolean.valueOf(elasticServiceFlag)*/) {
			
		}else {
			response = searchBDO.searchLinkageComponent(searchCriteria, bindingresult,reqParams,request);
		}
		return response;
	}
	public Response<Object> getLinkageComponent(Map<String,String> reqParams,HttpServletRequest request) throws Exception {
		return searchBDO.getLinkageComponent(reqParams,request);
	}
	
	public Response<Object> searchduplicateComponent(ComponentBean component,BindingResult bindingresult,HttpServletRequest request) throws Exception {
		Response<Object> response=new Response<Object>();
			response = searchBDO.searchduplicateComponent(component,bindingresult,request);
		return response;
	}
	public Response<Object> searchDuplicateComponentByKey(String key,ComponentBean component,BindingResult bindingresult,HttpServletRequest request) throws Exception {
		Response<Object> response=new Response<Object>();
			response = searchBDO.searchDuplicateComponentByKey(key,component,bindingresult,request);
		return response;
	}
	
}
